#include "stdafx.h"
#include <iostream>
#include <fstream>
#include "program.h"

using namespace std;

using namespace simple_shapes;

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		cout << "Incorrect command line! "
		"Waited: command infile outfile" << endl;
		return 0;
	}

	ifstream in(argv[1]);
	
	if (!in.is_open())
	{
		cout << "File " << argv[1] << " cannot be opened!" << endl;
		return 0;
	}

	ofstream out(argv[2]);
	cout << "Start" << endl;
	List c;
	c.In(in);
	out << "Filled container. " << endl;
	c.Sort();
	c.Out(out);
	c.OutAphorisms(out);
	c.MultiMethod(out);
	c.Clear();
	out << "Empty container. " << endl;
	c.Out(out);
	cout << "Stop" << endl;
	return 0;
}

